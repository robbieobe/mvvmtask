package com.example.mvvmtask.Db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.mvvmtask.Model.Product;


@Database(entities = {Product.class}, version = 4)
public abstract class ProductDatabase extends RoomDatabase {
    private static final String DATABASE_NAME="ProductDb";

    public abstract ProductDAO productDAO();

    private static volatile ProductDatabase INSTANCE;

    public static ProductDatabase getInstance(Context context){
        if(INSTANCE == null)
        {
            synchronized (ProductDatabase.class){
                if(INSTANCE == null)
                {
                    INSTANCE= Room.databaseBuilder(context,ProductDatabase.class,
                            DATABASE_NAME)
                            .addCallback(callback)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    static Callback callback=new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateAsyncTask(INSTANCE);
        }
    };
    static class PopulateAsyncTask extends AsyncTask<Void,Void,Void>
    {
        private ProductDAO productDAO;
        PopulateAsyncTask(ProductDatabase productDatabase)
        {
            productDAO=productDatabase.productDAO();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            productDAO.deleteAll();
            return null;
        }
    }
}