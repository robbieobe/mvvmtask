package com.example.mvvmtask.Db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.mvvmtask.Model.Product;

import java.util.List;

@Dao
public interface ProductDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Product> productList);

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveProducts(List<Product> products);

    @Query("SELECT * FROM product")
    LiveData<List<Product>> getAllProducts();

    @Query("DELETE FROM product")
    void deleteAll();
}
