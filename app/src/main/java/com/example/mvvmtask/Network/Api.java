package com.example.mvvmtask.Network;

import com.example.mvvmtask.Model.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    @GET("products.json?brand=maybelline")
    Call<List<Product>> getAllProducts();
}