package com.example.mvvmtask.Repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.mvvmtask.Db.ProductDAO;
import com.example.mvvmtask.Db.ProductDatabase;
import com.example.mvvmtask.Model.Product;

import java.util.List;


public class ProductRepository {
    private ProductDatabase database;
    private LiveData<List<Product>> getAllProducts;

    public ProductRepository(Application application)
    {
        database = ProductDatabase.getInstance(application);
        getAllProducts=database.productDAO().getAllProducts();
    }

    public void insert(List<Product> productList){
        new InsertAsyncTask(database).execute(productList);
    }

    public LiveData<List<Product>> getAllProducts()
    {
        return getAllProducts;
    }

    static class InsertAsyncTask extends AsyncTask<List<Product>,Void,Void> {
        private ProductDAO productDAO;
        InsertAsyncTask(ProductDatabase productDatabase)
        {
            productDAO=productDatabase.productDAO();
        }
        @Override
        protected Void doInBackground(List<Product>... lists) {
            productDAO.insert(lists[0]);
            return null;
        }
    }
}
