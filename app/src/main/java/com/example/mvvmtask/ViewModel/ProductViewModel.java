package com.example.mvvmtask.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.mvvmtask.Model.Product;
import com.example.mvvmtask.Repository.ProductRepository;

import java.util.List;

public class ProductViewModel extends AndroidViewModel {

    private ProductRepository productRepository;
    private LiveData<List<Product>> getAllProducts;

    public ProductViewModel(@NonNull Application application) {
        super(application);
        productRepository=new ProductRepository(application);
        getAllProducts=productRepository.getAllProducts();
    }

    public void insert(List<Product> list)
    {
        productRepository.insert(list);
    }

    public LiveData<List<Product>> getAllProducts()
    {
        return getAllProducts;
    }


}
