package com.example.mvvmtask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import com.example.mvvmtask.Adapter.ProductAdapter;
import com.example.mvvmtask.Model.Product;
import com.example.mvvmtask.Network.Retrofit;
import com.example.mvvmtask.Repository.ProductRepository;
import com.example.mvvmtask.ViewModel.ProductViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static ProductRepository productRespository;
    private static RecyclerView recyclerView;
    private List<Product> productList;
    private static ProductAdapter productAdapter;
    private static TextView lastUpdate;
    private static ProductViewModel productViewModel;

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        productRespository=new ProductRepository(getApplication());
        productList=new ArrayList<>();
        productAdapter=new ProductAdapter(this,productList);

        productViewModel=new ViewModelProvider(this).get(ProductViewModel.class);
        lastUpdate = findViewById(R.id.lastUpdate);

        handler.post(r);
    }
    Runnable r = new Runnable() {
        @Override
        public void run() {
            makeApiCall();
            productViewModel.getAllProducts().observe(MainActivity.this, productList -> {
                recyclerView.setAdapter(productAdapter);
                productAdapter.getAllProducts(productList);
            });
            handler.removeCallbacks(r);
            // fetch every 30 minutes
            handler.postDelayed(r,1800000);
        }
    };

    public static void makeApiCall() {
        Retrofit retrofit=new Retrofit();
        Call<List<Product>> call=retrofit.api.getAllProducts();
        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if(response.isSuccessful())
                {
                    productRespository.insert(response.body());
                    lastUpdate.setText("Last Update: " + Calendar.getInstance().getTime());
                    Log.d("main", "onResponse successful");
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d("Retrofit", t.getMessage());
            }
        });

    }
}