package com.example.mvvmtask.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mvvmtask.Model.Product;

import java.util.List;

import com.example.mvvmtask.R;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private Context context;
    private List<Product> productList;

    public ProductAdapter(Context context, List<Product> actorList) {
        this.context =  context;
        this.productList = productList;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product=productList.get(position);
        holder.id.setText("id-" + product.getId());
        holder.name.setText("Name: "+product.getName());
        holder.product_type.setText("Product type: "+product.getProduct_type());


    }

    public void getAllProducts(List<Product> productList)
    {
        this.productList=productList;
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {

        int a ;

        if(productList != null && !productList.isEmpty()) {
            a = productList.size();
        }
        else {
            a = 0;
        }

        return a;
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder{
        public TextView id,name,product_type;
        public ImageView image;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            id=itemView.findViewById(R.id.uid);
            name=itemView.findViewById(R.id.name);
            product_type=itemView.findViewById(R.id.product_type);
        }
    }
}
