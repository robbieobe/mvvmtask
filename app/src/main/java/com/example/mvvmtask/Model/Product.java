package com.example.mvvmtask.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "product")
public class Product {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "product_type")
    private String product_type;


    public Product(String name, String product_type) {
        this.name = name;
        this.product_type = product_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int uid) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }


    @Override
    public String toString() {
        return "Product{" +
                ", name='" + name + '\'' +
                ", product_type='" + product_type + '\'' +
                '}';
    }
}
